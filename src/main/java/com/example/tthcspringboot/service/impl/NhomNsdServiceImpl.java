package com.example.tthcspringboot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.tthcspringboot.entity.HtNhomNsd;
import com.example.tthcspringboot.repository.NhomNsdRepository;
import com.example.tthcspringboot.service.NhomNsdService;

@Service
public class NhomNsdServiceImpl implements NhomNsdService {

	@Autowired
	private NhomNsdRepository repo;

	@Override
	public void delete(HtNhomNsd entity) {
		repo.deleteById(entity);
	}

}
