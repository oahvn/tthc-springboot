package com.example.tthcspringboot.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.tthcspringboot.entity.HtNsd;
import com.example.tthcspringboot.pagination.Pageable;
import com.example.tthcspringboot.repository.SearchRepository;
import com.example.tthcspringboot.repository.UserRepository;
import com.example.tthcspringboot.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository repo;
	@Autowired
	private PasswordEncoder bcrypt;
	@Autowired
	private SearchRepository search;

	@SuppressWarnings("unchecked")
	@Override
	/*
	 * Save nsd return message and http status when it has errors
	 */
	public <T> T save(HtNsd user) {
		Map<String, String> errors = new HashMap<String, String>();
		if (user == null) {
			errors.put("User", "User not exists");
		} else {

			if (user.getTenDangNhap() == null || !user.getTenDangNhap().matches("[\\w]{6,100}")) {
				errors.put("username", "Invalid username");
			} else {
				HtNsd existsUsername = repo.findByTenDangNhap(user.getTenDangNhap());
				if (existsUsername != null) {
					errors.put("existsUsername", "Existing username");
				}
			}
			if (user.getMatKhau() == null || !user.getMatKhau().matches("[\\w\\W]{6,100}")) {
				errors.put("password", "Invalid password");
			}
			if (user.getEmail() == null || !user.getEmail().matches("^\\w{6,100}[@]{1}\\w{1,30}[.]{1}\\w{2,18}")) {
				errors.put("email", "Invalid email");
			}
			if (user.getDienThoai() == null
					|| !user.getDienThoai().matches("^(086|096|097|098|032|033|034|035|036|037|038|039)[0-9]{7}$")) {
				errors.put("phonenumber", "Invalid phone number");
			}
		}
		if (!errors.isEmpty()) {
			return (T) new ResponseEntity<T>((T) errors, HttpStatus.BAD_REQUEST);
		}
		user.setId(UUID.randomUUID().toString());
		user.setMatKhau(user.getMatKhau());
		user.setNgayTao(new Date());

		if (!user.getHtNhomNsdsForHtNsdId().isEmpty()) {
			user.getHtNhomNsdsForHtNsdId().forEach(nhomnsd -> {
				nhomnsd.setId(UUID.randomUUID().toString());
				nhomnsd.setHtNsdByHtNsdId(user);
			});
		}

		return (T) new ResponseEntity<T>((T) repo.save(user), HttpStatus.OK);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	/*
	 * Update nsd Return message and http status when it has errors Return nsd
	 * if update success
	 */
	public <T> T update(HtNsd user) {
		Map<String, String> errors = new HashMap<String, String>();
		HtNsd exists = null;
		if (user == null) {
			errors.put("User", "User not exists");
		} else {
			Optional<HtNsd> optional = repo.findById(user.getId());
			if (optional.isPresent()) {
				exists = optional.get();
			} else {
				errors.put("Error", "Can not find user by id");
			}
			if (user.getTenDangNhap() == null || !user.getTenDangNhap().matches("[\\w]{6,100}")) {
				errors.put("username", "Invalid username");
			} else {
				if (exists != null) {
					if (!exists.getTenDangNhap().equals(user.getTenDangNhap())) {
						HtNsd existsUsername = repo.findByTenDangNhap(user.getTenDangNhap());
						if (existsUsername != null) {
							errors.put("exists", "Existing username");
						}
					}
				}
			}
			if (user.getMatKhau() == null || !user.getMatKhau().matches("[\\w\\W]{6,100}")) {
				errors.put("password", "Invalid password");
			}
			if (user.getEmail() == null || !user.getEmail().matches("^\\w{6,100}[@]{1}\\w{1,30}[.]{1}\\w{2,18}")) {
				errors.put("email", "Invalid email");
			}
			if (user.getDienThoai() == null
					|| !user.getDienThoai().matches("^(086|096|097|098|032|033|034|035|036|037|038|039)[0-9]{7}$")) {
				errors.put("phonenumber", "Invalid phone number");
			}
		}
		if (!errors.isEmpty()) {
			return (T) new ResponseEntity<T>((T) errors, HttpStatus.BAD_REQUEST);
		}

		// Update
		exists.setTenDangNhap(user.getTenDangNhap());
		exists.setMatKhau(user.getMatKhau());
		exists.setNgayCapNhat(new Date());
		exists.setEmail(user.getEmail());
		exists.setDienThoai(user.getDienThoai());
		exists.getHtNhomNsdsForHtNsdId().clear();
		//
		if (!user.getHtNhomNsdsForHtNsdId().isEmpty()) {
			user.getHtNhomNsdsForHtNsdId().forEach(nhomnsd -> {
				nhomnsd.setId(UUID.randomUUID().toString());
				nhomnsd.setNgayTao(new Date());
				nhomnsd.setHtNsdByHtNsdId(user);
			});
		}

		/**
		 * 
		 * ***************************NOTE*****************************************
		 */
		exists.getHtNhomNsdsForHtNsdId().addAll(user.getHtNhomNsdsForHtNsdId());
		/**
		 * 
		 * ***************************NOTE*****************************************
		 */

		return (T) new ResponseEntity<T>((T) repo.save(exists), HttpStatus.OK);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T deleteById(String id) {
		if (id.matches("[\\W]")) {
			return (T) new ResponseEntity<T>((T) "Invalid id", HttpStatus.BAD_REQUEST);
		} else {
			Optional<HtNsd> optional = repo.findById(id);
			if (optional.isPresent()) {
				repo.delete(optional.get());
				return (T) new ResponseEntity<T>((T) "Deleted", HttpStatus.OK);
			} else {
				return (T) new ResponseEntity<T>((T) "Id not found", HttpStatus.BAD_REQUEST);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T findById(String id) {
		if (id == null || id.equals("") || id.matches("[\\W]")) {
			return (T) new ResponseEntity<T>((T) "User not found", HttpStatus.BAD_REQUEST);
		}
		Optional<T> option = (Optional<T>) repo.findById(id);
		if (option.isPresent()) {
			return (T) new ResponseEntity<T>(option.get(), HttpStatus.OK);
		} else {
			return (T) new ResponseEntity<T>((T) "User not found", HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public Map<String, Object> findBySimilar(HtNsd similar, Pageable pageable) {
		Map<String, Object> map = new HashMap<>();
		map.put("list", search.findBySimilar(similar, pageable));
		int totalRecords = (int) search.countBySimilar(similar);
		int totalPages = (totalRecords % pageable.getTotal() == 0) ? totalRecords / pageable.getTotal()
				: totalRecords / pageable.getTotal() + 1;
		pageable.setTotalPages(totalPages);
		map.put("pageable", pageable);
		return map;
	}

	@Override
	public Map<String, Object> findByLike(String similar, Pageable pageable) {
		Map<String, Object> map = new HashMap<>();
		map.put("list", search.findByLike(similar, pageable));
		int totalRecords = (int) search.countByLike(similar);
		int totalPages = (totalRecords % pageable.getTotal() == 0) ? totalRecords / pageable.getTotal()
				: totalRecords / pageable.getTotal() + 1;
		pageable.setTotalPages(totalPages);
		map.put("pageable", pageable);
		return map;
	}

	@Override
	public boolean findByTenDangNhapAndMatKhau(String tenDangNhap, String matKhau) {
		if (!tenDangNhap.matches("[\\w]{6,100}")) {
			return false;
		} else if (!matKhau.matches("(.+){6,100}")) {
			return false;
		} else {
			HtNsd nsd = repo.findByTenDangNhapAndMatKhau(tenDangNhap, matKhau);
			if (nsd != null) {
				return true;
			} else {
				return false;
			}
		}
	}

	@Override
	public HtNsd findByTenDangNhap(String tenDangNhap) {
		if (tenDangNhap == null || !tenDangNhap.trim().matches("[\\w]{6,100}")) {
			return null;
		} else {
			HtNsd user = repo.findByTenDangNhap(tenDangNhap);
			if (user == null) {
				return null;
			} else {
				return user;
			}
		}
	}

}
