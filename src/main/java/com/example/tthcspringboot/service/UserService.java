package com.example.tthcspringboot.service;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.example.tthcspringboot.entity.HtNsd;
import com.example.tthcspringboot.pagination.Pageable;

@Service
public interface UserService {
	public <T> T save(HtNsd user);
	public <T> T update(HtNsd user);
	public <T> T deleteById(String id);
	public boolean findByTenDangNhapAndMatKhau(String tenDangNhap,String matKhau);
	public HtNsd findByTenDangNhap(String tenDangNhap);
	public <T> T findById(String id);
	public Map<String, Object> findBySimilar(HtNsd similar, Pageable pageable);
	public Map<String, Object> findByLike(String similar, Pageable pageable);
}
