package com.example.tthcspringboot.service;

import org.springframework.stereotype.Service;

import com.example.tthcspringboot.entity.HtNhomNsd;

@Service
public interface NhomNsdService {
	public void delete(HtNhomNsd entity);
}
