package com.example.tthcspringboot.security;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.example.tthcspringboot.entity.HtNsd;

public class NsdDetails implements UserDetails {

	private static final long serialVersionUID = 1L;
	private HtNsd nsd;

	public NsdDetails(HtNsd nsd) {
		super();
		this.nsd = nsd;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();

		if (nsd.getHtNhomNsdsForHtNsdId() != null && !nsd.getHtNhomNsdsForHtNsdId().isEmpty()) {
			nsd.getHtNhomNsdsForHtNsdId().forEach(nhomnsd -> {
				String ma = nhomnsd.getHtNhom().getMa();
				roles.add(new SimpleGrantedAuthority(ma));
			});
		}

		return roles;
	}

	@Override
	public String getPassword() {
		return nsd.getMatKhau();
	}

	@Override
	public String getUsername() {
		return nsd.getTenDangNhap();
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

}
