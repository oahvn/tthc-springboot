package com.example.tthcspringboot.pagination;

public class Pageable {
	private short page;
	private byte total;
	private long totalPages;

	public short getPage() {
		return page;
	}

	public void setPage(short page) {
		this.page = page;
	}

	public byte getTotal() {
		return total;
	}

	public void setTotal(byte total) {
		this.total = total;
	}

	public long getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(long totalPages) {
		this.totalPages = totalPages;
	}

}
