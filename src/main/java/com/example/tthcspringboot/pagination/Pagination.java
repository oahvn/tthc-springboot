package com.example.tthcspringboot.pagination;

public class Pagination<T> {
	private T similar;
	private Pageable pageable;

	public T getSimilar() {
		return similar;
	}

	public void setSimilar(T similar) {
		this.similar = similar;
	}

	public Pageable getPageable() {
		return pageable;
	}

	public void setPageable(Pageable pageable) {
		this.pageable = pageable;
	}

}
