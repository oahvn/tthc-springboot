package com.example.tthcspringboot.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.tthcspringboot.security.JwtService;
import com.example.tthcspringboot.security.LoginRequest;
import com.example.tthcspringboot.security.SecurityResponse;
import com.example.tthcspringboot.service.UserService;

@RestController
@RequestMapping("/api")
public class SecurityController {

	@Autowired
	private JwtService jwtService;

	@Autowired
	private UserService userService;

	@PostMapping("/login")
	public ResponseEntity<SecurityResponse> login(@RequestBody LoginRequest account) {
		SecurityResponse response = new SecurityResponse();
		if (account == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		if (userService.findByTenDangNhapAndMatKhau(account.getUsername(), account.getPassword())) {
			response.setStatus("200");
			response.setToken(jwtService.generateTokenLogin(account.getUsername(),
					new Date(System.currentTimeMillis() + 1800000)));
			response.setUsername(account.getUsername());
			return new ResponseEntity<SecurityResponse>(response, HttpStatus.OK);
		} else {
			response.setStatus("Invalid username or password");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	}
}
