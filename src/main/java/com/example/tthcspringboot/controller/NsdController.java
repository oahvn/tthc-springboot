package com.example.tthcspringboot.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.tthcspringboot.entity.HtNsd;
import com.example.tthcspringboot.pagination.Pagination;
import com.example.tthcspringboot.service.UserService;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/nsd")
public class NsdController {

	@Autowired
	private UserService service;

	@PostMapping("/save")
	public ResponseEntity<Object> save(@RequestBody HtNsd user) {
		return service.save(user);
	}

	@PostMapping("/update")
	public ResponseEntity<Object> update(@RequestBody HtNsd user) {
		return service.update(user);
	}

	@GetMapping("/delete/{id}")
	public ResponseEntity<Object> delete(@PathVariable String id) {
		return service.deleteById(id);
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Object> findById(@PathVariable String id) {
		return service.findById(id);
	}

	@GetMapping("/find")
	public Map<String, Object> find(@RequestBody Pagination<HtNsd> pagination) {
		return service.findBySimilar(pagination.getSimilar(), pagination.getPageable());
	}

	@GetMapping("/search")
	public Map<String, Object> search(@RequestBody Pagination<String> pagination) {
		return service.findByLike(pagination.getSimilar(), pagination.getPageable());
	}
}
