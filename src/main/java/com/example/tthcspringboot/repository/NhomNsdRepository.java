package com.example.tthcspringboot.repository;

import org.springframework.stereotype.Repository;

import com.example.tthcspringboot.entity.HtNhomNsd;

@Repository
public interface NhomNsdRepository {
	public void deleteById(HtNhomNsd entity);
}
