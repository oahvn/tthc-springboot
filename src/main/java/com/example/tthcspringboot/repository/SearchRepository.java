package com.example.tthcspringboot.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.tthcspringboot.entity.HtNsd;
import com.example.tthcspringboot.pagination.Pageable;

@Repository
public interface SearchRepository {
	public List<HtNsd> findBySimilar(HtNsd similar, Pageable pageable);
	public List<HtNsd> findByLike(String similar, Pageable pageable);
	public long countBySimilar(HtNsd similar);
	public long countByLike(String similar);
}
