package com.example.tthcspringboot.repository.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.tthcspringboot.entity.HtNsd;
import com.example.tthcspringboot.pagination.Pageable;
import com.example.tthcspringboot.repository.SearchRepository;

@Repository
@Transactional
public class SearchRepositoryImpl implements SearchRepository {

	@Autowired
	private EntityManager em;

	private boolean validateString(Object value) {
		if (value != null && !value.toString().trim().equals("")) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HtNsd> findBySimilar(HtNsd similar, Pageable pageable) {
		List<HtNsd> list = new ArrayList<HtNsd>();
		String jpql = "SELECT t FROM HtNsd t";
		String search = "";
		Map<String, Object> params = new HashMap<String, Object>();

		if (similar != null) {
			if (validateString(similar.getTenDangNhap())) {
				search += " t.tenDangNhap = :tenDangNhap AND";
				params.put("tenDangNhap", similar.getTenDangNhap());
			}
			if (validateString(similar.getEmail())) {
				search += " t.email = :email AND";
				params.put("email", similar.getEmail());
			}
			if (validateString(similar.getDienThoai())) {
				search += " t.dienThoai = :dienThoai AND";
				params.put("dienThoai", similar.getDienThoai());
			}
			if (validateString(similar.getNgayCapNhat())) {
				search += " to_date(t.ngayCapNhat,'dd-MON-yy') = to_date(:ngayCapNhat,'dd-MON-yy') AND";
				params.put("ngayCapNhat", similar.getNgayCapNhat());
			}
			if (!search.equals("")) {
				search = search.trim().substring(0, search.length() - 4);
				jpql += " WHERE (" + search + ")";
			}
		}

		Query query = em.createQuery(jpql);

		if (similar != null && !params.isEmpty()) {
			params.forEach((key, value) -> {
				query.setParameter(key, value);
			});
		}

		if (pageable != null) {
			int startPosition = (pageable.getPage() - 1) * pageable.getTotal();
			query.setFirstResult(startPosition);
			query.setMaxResults(pageable.getTotal());
		}

		list = query.getResultList();
		return list;
	}

	@Override
	public long countBySimilar(HtNsd similar) {
		String jpql = "SELECT COUNT(*) FROM HtNsd t";
		String search = "";
		Map<String, Object> params = new HashMap<String, Object>();

		if (similar != null) {
			if (validateString(similar.getTenDangNhap())) {
				search += " t.tenDangNhap = :tenDangNhap AND";
				params.put("tenDangNhap", similar.getTenDangNhap());
			}
			if (validateString(similar.getEmail())) {
				search += " t.email = :email AND";
				params.put("email", similar.getEmail());
			}
			if (validateString(similar.getDienThoai())) {
				search += " t.dienThoai = :dienThoai AND";
				params.put("dienThoai", similar.getDienThoai());
			}
			if (validateString(similar.getNgayCapNhat())) {
				search += " to_date(t.ngayCapNhat,'dd-MON-yy') = to_date(:ngayCapNhat,'dd-MON-yy') AND";
				params.put("ngayCapNhat", similar.getNgayCapNhat());
			}
			if (!search.equals("")) {
				search = search.substring(0, search.length() - 4);
				jpql += " WHERE (" + search + ")";
			}
		}

		Query query = em.createQuery(jpql);

		if (similar != null && !params.isEmpty()) {
			params.forEach((key, value) -> {
				query.setParameter(key, value);
			});
		}

		long count = (long) query.getResultList().get(0);

		return count;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HtNsd> findByLike(String similar, Pageable pageable) {
		List<HtNsd> list = new ArrayList<HtNsd>();
		String jpql = "SELECT t FROM HtNsd t";
		String search = "";
		Map<String, Object> params = new HashMap<String, Object>();

		if (similar != null) {
			if (validateString(similar)) {
				search += " t.tenDangNhap like :tenDangNhap OR";
				search += " t.email like :email OR";
				search += " t.dienThoai like :dienThoai OR";
				params.put("tenDangNhap", "%" + similar + "%");
				params.put("email", "%" + similar + "%");
				params.put("dienThoai", "%" + similar + "%");
			}
			if (!search.equals("")) {
				search = search.trim().substring(0, search.length() - 3);
				jpql += " WHERE (" + search + ")";
			}
		}

		Query query = em.createQuery(jpql);

		if (similar != null && !params.isEmpty()) {
			params.forEach((key, value) -> {
				query.setParameter(key, value);
			});
		}

		if (pageable != null) {
			int startPosition = (pageable.getPage() - 1) * pageable.getTotal();
			query.setFirstResult(startPosition);
			query.setMaxResults(pageable.getTotal());
		} else {
			query.setFirstResult(1);
			query.setMaxResults(5);
		}

		list = query.getResultList();
		return list;
	}

	@Override
	public long countByLike(String similar) {
		String jpql = "SELECT COUNT(*) FROM HtNsd t";
		String search = "";
		Map<String, Object> params = new HashMap<String, Object>();

		if (similar != null) {
			if (validateString(similar)) {
				search += " t.tenDangNhap like :tenDangNhap OR";
				search += " t.email like :email OR";
				search += " t.dienThoai like :dienThoai OR";
				params.put("tenDangNhap", "%" + similar + "%");
				params.put("email", "%" + similar + "%");
				params.put("dienThoai", "%" + similar + "%");
			}
			if (!search.equals("")) {
				search = search.trim().substring(0, search.length() - 4);
				jpql += " WHERE (" + search + ")";
			}
		}

		Query query = em.createQuery(jpql);

		if (similar != null && !params.isEmpty()) {
			params.forEach((key, value) -> {
				query.setParameter(key, value);
			});
		}

		long count = (long) query.getResultList().get(0);

		return count;
	}

}
