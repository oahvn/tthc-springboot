package com.example.tthcspringboot.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.tthcspringboot.entity.HtNhomNsd;
import com.example.tthcspringboot.repository.NhomNsdRepository;

@Repository
@Transactional
public class NhomNsdRepositoryImpl implements NhomNsdRepository {

	@Autowired
	private EntityManager em;

	@Override
	public void deleteById(HtNhomNsd entity) {
		String jpql = "DELETE FROM HtNhomNsd t WHERE t.id = :id";
		Query query = em.createQuery(jpql);
		query.setParameter("id", entity.getId());
		query.executeUpdate();
	}

}
