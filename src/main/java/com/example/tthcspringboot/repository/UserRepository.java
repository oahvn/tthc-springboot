package com.example.tthcspringboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.tthcspringboot.entity.HtNsd;

@Repository
public interface UserRepository extends JpaRepository<HtNsd, String> {
	public HtNsd findByTenDangNhapAndMatKhau(String tenDangNhap, String matKhau);
	public HtNsd findByTenDangNhap(String tenDangNhap);
	public void deleteById(String id);
}
